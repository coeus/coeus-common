# Copyright 2022 ISEA - RWTH Aachen University
VERSION = (1, 0, 0)
# string created from tuple to avoid inconsistency
__version__ = ".".join([str(x) for x in VERSION])
