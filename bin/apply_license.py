#!/usr/bin/env python3
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import glob
import itertools
import os
import re
import shutil
import sys
import tempfile
from datetime import datetime
from datetime import timezone

import click


LICENSE_NOTICE = "{comment_start}Copyright [0-9]{{4}} ISEA - RWTH Aachen University"
LICENSE = (
    """{comment_start}Copyright {year} ISEA - RWTH Aachen University{comment_end}"""
)


FILE_TYPE_CONFIG = {
    ".py": {
        "comment_start": "# ",
        "comment_middle": "# ",
        "comment_end": "",
        "empty_lines": 0,
    },
    ".js": {
        "comment_start": "/* ",
        "comment_middle": " * ",
        "comment_end": " */",
        "empty_lines": 1,
    },
    ".css": {
        "comment_start": "/* ",
        "comment_middle": " * ",
        "comment_end": " */",
        "empty_lines": 1,
    },
    ".scss": {
        "comment_start": "/* ",
        "comment_middle": " * ",
        "comment_end": " */",
        "empty_lines": 1,
    },
    ".vue": {
        "comment_start": "<!-- ",
        "comment_middle": "   - ",
        "comment_end": " -->",
        "empty_lines": 1,
    },
    ".html": {
        "comment_start": "{# ",
        "comment_middle": " # ",
        "comment_end": " #}",
        "empty_lines": 1,
    },
}


@click.command()
@click.argument("paths", type=click.Path(exists=True), nargs=-1)
@click.option(
    "-x",
    "--exclude",
    multiple=True,
    help="A path to exclude. A file will be excluded if it starts with the given path."
    " Can be specified more than once.",
)
@click.option("-a", "--apply", is_flag=True, help="Apply all missing license headers.")
def apply_license(paths, exclude, apply):
    """Check for license headers in one or multiple files.

    The given PATHS can be either files and/or paths that will be searched recursively
    for suitable file types to apply the header on. The supported file types are: *.py,
    *.js, *.css, *.scss, *.vue and *.html.
    """
    file_lists = []
    found_missing_headers = False
    print(paths)
    # Collect all files to check.
    for path in paths:
        if os.path.isfile(path):
            file_lists.append([path])
        else:
            for file_type in FILE_TYPE_CONFIG:
                file_lists.append(
                    glob.iglob(
                        os.path.join(path, "**", f"*{file_type}"), recursive=True
                    )
                )

    for file in itertools.chain(*file_lists):
        found_missing_header = False

        # Ignore globbed directories.
        if os.path.isdir(file):
            continue

        # Check for excluded paths.
        skip_file = False
        for e in exclude:
            if file.startswith(e):
                skip_file = True
                break

        if skip_file:
            continue

        # Ignore files with non-matching extensions.
        _, file_type = os.path.splitext(file)
        if file_type not in FILE_TYPE_CONFIG:
            continue

        # Check the file for an existing header.
        with open(file, mode="r+", encoding="utf-8") as f:
            config = FILE_TYPE_CONFIG[file_type]

            # Create the fitting license header for the current file.
            license_header = LICENSE.format(
                year=datetime.now(timezone.utc).year,
                comment_start=config["comment_start"],
                comment_middle=config["comment_middle"],
                comment_end=config["comment_end"],
            )
            license_header = "\n".join(
                [line.rstrip() for line in license_header.split("\n")]
            )

            first_line = f.readline()
            license_notice = re.compile(
                LICENSE_NOTICE.format(comment_start=re.escape(config["comment_start"]))
            )

            # Check the first line using a regex because of the potentially differing
            # year. If it matches, continue with the rest of the license header.
            if not license_notice.match(first_line):
                found_missing_header = found_missing_headers = True
            else:
                for line_f, line_header in itertools.zip_longest(
                    f, license_header.split("\n")[1:]
                ):
                    # Finished checking the whole license header.
                    if line_header is None:
                        break

                    if not line_f == f"{line_header}\n":
                        found_missing_header = found_missing_headers = True
                        break

        if found_missing_header:
            if apply:
                with open(file, encoding="utf-8") as f:
                    file_content = f.read()

                # Create a new file in the same directory with the header and file
                # content, then replace the old one.
                # pylint: disable=consider-using-with
                tmp_file = tempfile.NamedTemporaryFile(
                    mode="w", dir=os.path.dirname(file), delete=False, newline="\n"
                )

                try:
                    tmp_file.write(license_header)

                    if file_content:
                        tmp_file.write("\n" * (config["empty_lines"] + 1))
                    else:
                        # Empty file, just write a single newline.
                        tmp_file.write("\n")

                    tmp_file.write(file_content)
                    tmp_file.close()

                    shutil.move(tmp_file.name, file)

                    click.echo(f"Applied license header to '{file}'.")
                except Exception as e:
                    click.secho(str(e), fg="red")

                    try:
                        os.remove(tmp_file.name)
                    except FileNotFoundError:
                        pass

            else:
                click.echo(f"No license header found in '{file}'.")

    if found_missing_headers and not apply:
        click.echo("\nRerun with -a/--apply to apply the missing license headers.")
        sys.exit(1)


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    apply_license()
