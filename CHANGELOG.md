## 1.0.0 (2022-03-08)


### Features

* move NodeState in coeus-common ([9a971d4](https://git.isea.rwth-aachen.de/Personal-Projects/ESS/moh/modellkoordination/coeus/coeus-common/commit/9a971d413c074efe1eec4750fa30380fd6f8e2e9))
