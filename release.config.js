/* Copyright 2021 ISEA - RWTH Aachen University
 *
 * Licensed under the 3-Clause BSD License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://opensource.org/licenses/BSD-3-Clause
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE. */
const gitlabUrl = 'https://git.isea.rwth-aachen.de'
const gitlabApiPathPrefix = '/api/v4'
const config = {
    preset: "conventionalcommits",
    npmPublish: false,
    branches: ["master"],
    plugins: [
        ["@semantic-release/commit-analyzer", {
            releaseRules: [
                {"type": "refactor", "release": "patch"},
                {"type": "style", "release": "patch"},
                {"type": "patch", "release": "patch"}
            ]
        }],
        ["@semantic-release/release-notes-generator", {
            presetConfig: {
                types: [
                    {
                        "type": "fix",
                        "section": "Bug Fixes",
                    },
                    {
                        "type": "patch",
                        "section": "Patches",
                    },
                    {
                        "type": "feat",
                        "section": "Features"
                    },
                    {
                        "type": "refactor",
                        "section": "Internal - Refactor",
                        "hidden": false
                    },
                    {
                        "type": "style",
                        "section": "Internal - Style",
                        "hidden": false
                    }
                ]
            }
        }],
        ['@semantic-release/changelog', {changelogFile: "CHANGELOG.md"}],
        ["@semantic-release/exec",
            {
                prepareCmd: "./bin/updateversion.py ${nextRelease.version} src/coeus_common/__init__.py",
            }],
        ['@semantic-release/git',
            {
                assets: ["CHANGELOG.md", 'src/coeus_common/__init__.py'],
                message: 'chore(release): ${nextRelease.version}\n\n${nextRelease.notes}',
            }],
    ]
}

module.exports = config
