if [ $CI_BUILD_BEFORE_SHA == "0000000000000000000000000000000000000000" ]; then
  npx commitlint --from=HEAD~1
else
  npx commitlint --from=$CI_BUILD_BEFORE_SHA;
fi;
